#!/usr/bin/env python
#! coding=utf-8
# PACKAGE = 'msf_updates'
# import roslib;roslib.load_manifest(PACKAGE)
import rospy
from geometry_msgs.msg import TransformStamped,TwistStamped,PoseStamped,Vector3Stamped
from sensor_fusion_comm.msg import DoubleArrayStamped


def pubVrpn(msg):
    message = TransformStamped()
    message.header.stamp.secs = msg.header.stamp.secs
    message.header.stamp.nsecs = msg.header.stamp.nsecs
    message.transform.translation.x = msg.pose.position.x
    message.transform.translation.y = msg.pose.position.y
    message.transform.translation.z = msg.pose.position.z

    message.transform.rotation.x = msg.pose.orientation.x
    message.transform.rotation.y = msg.pose.orientation.y
    message.transform.rotation.z = msg.pose.orientation.z
    message.transform.rotation.w = msg.pose.orientation.w

    vrpn_pose_pub.publish(message)

def localCallback(msg):
    global init,lastTime,lastX,lastY,lastZ,lastSec,lastNsec,cnt
    global x,y,z
    #PoseStamped --> TransformStamped
    pubVrpn(msg)
    if init == 0:
        lastTime = msg.header.stamp.to_nsec()
        lastSec = msg.header.stamp.secs
        lastNsec = msg.header.stamp.nsecs
        lastX =  msg.pose.position.x
        lastY =  msg.pose.position.y
        lastZ =  msg.pose.position.z
        init = 1
    else:
        if (msg.header.stamp.to_nsec() - lastTime)*1e-09 > 0.001:
            if cnt < 10:
                x += (msg.pose.position.x - lastX)/((msg.header.stamp.to_nsec() - lastTime)*1e-09)
                y += (msg.pose.position.y - lastY)/((msg.header.stamp.to_nsec() - lastTime)*1e-09)
                z += (msg.pose.position.z - lastZ)/((msg.header.stamp.to_nsec() - lastTime)*1e-09)
                cnt += 1
                # print 'cnt : ',cnt
                # print "x : " , x
                # print "y : " , y
                # print "z : " , z
                # print msg.pose.position.x - lastX, (msg.header.stamp.to_nsec() - lastTime)*1e-09
                # print msg.pose.position.y - lastY, (msg.header.stamp.to_nsec() - lastTime)*1e-09
                # print msg.pose.position.z - lastZ, (msg.header.stamp.to_nsec() - lastTime)*1e-09

            else:
                message = TwistStamped()
                message.header.stamp.secs = lastSec
                message.header.stamp.nsecs = lastNsec
                message.twist.linear.x = x/10.0
                message.twist.linear.y = y/10.0
                message.twist.linear.z = z/10.0

                velocity_pub.publish(message)
                cnt = 0
                x = y = z = 0.

            lastX = msg.pose.position.x
            lastY = msg.pose.position.y
            lastZ = msg.pose.position.z
            lastTime = msg.header.stamp.to_nsec()
            lastSec = msg.header.stamp.secs
            lastNsec = msg.header.stamp.nsecs

def msfCallback(msg):
    message = Vector3Stamped()
    message.header.stamp = msg.header.stamp
    message.vector.x = msg.data[3]
    message.vector.y = msg.data[4]
    message.vector.z = msg.data[5]
    msf_vel_pub.publish(message)

lastTime = 0
lastX = lastY = lastZ = 0
lastSec = lastNsec =0
init = 0
cnt = 0
x = y = z =0

rospy.init_node('velocity_publish')
vrpn_pose_sub = rospy.Subscriber('/vrpn/vision_pose/pose',PoseStamped,localCallback)
velocity_pub = rospy.Publisher('/vrpn/velocity',TwistStamped,queue_size=10)
#transfrom message format
vrpn_pose_pub = rospy.Publisher('/vrpn/transfrom/pose',TransformStamped,queue_size=10)
msf_state_sub = rospy.Subscriber('/msf_core/state_out',DoubleArrayStamped,msfCallback)
msf_vel_pub = rospy.Publisher('/msf_core/velocity',Vector3Stamped,queue_size=10)
rospy.spin()
